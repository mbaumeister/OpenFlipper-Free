#ifndef QUADMODIFIER_H
#define QUADMODIFIER_H

#include <memory>
#include <OpenFlipper/common/Types.hh>
#include <ObjectTypes/PolyMesh/PolyMesh.hh>
#include <OpenMesh/Core/Utils/Property.hh>

#include "QuadCycle.hh"

struct Tangle
{
    std::vector< OpenMesh::HalfedgeHandle > boundary_;
    unsigned long area_;
    bool success_;

    static Tangle errorTangle();

    bool operator==(const Tangle &t);
    bool operator<(const Tangle &t) const;
};

class QuadModifier
{
  public:
    OpenMesh::HPropHandleT< int > dualLoopIndex_; // value +k or -k for the k-th loop (with direction)
    OpenMesh::MPropHandleT< std::vector< std::vector< OpenMesh::HalfedgeHandle > > > dualLoops_; // Loop numbering starts at 1 !!
    OpenMesh::MPropHandleT< std::vector< QuadCycle > > cycle2_;
    OpenMesh::MPropHandleT< std::vector< QuadCycle > > cycle4_;
    OpenMesh::MPropHandleT< std::vector< QuadCycle > > cycle6_;
    OpenMesh::MPropHandleT< std::set< Tangle > > tangles_;
    OpenMesh::MPropHandleT< std::vector< std::vector< OpenMesh::HalfedgeHandle > > > homologyBasis_;
    OpenMesh::MPropHandleT< std::vector< std::vector< std::vector< std::vector< unsigned long > > > > > spirals_; // [dualLoopIndex][homGenIndex][spiralIndex] -> two positions in homology generator
    OpenMesh::MPropHandleT< std::vector< std::vector< OpenMesh::HalfedgeHandle > > > spiralPaths_;

    QuadModifier( PolyMesh& _mesh);
    ~QuadModifier();

  private:
    void computeDualLoops();
    void computeTangles();
    void computeCycles();
    void analyseMesh();

    Tangle growTangle( OpenMesh::HalfedgeHandle first, OpenMesh::HalfedgeHandle second);
    bool flood( std::vector<OpenMesh::HalfedgeHandle>& waveFront, std::vector<OpenMesh::FaceHandle>& submergedFaces, OpenMesh::VertexHandle initialVertex);
    void waveCollapse( std::vector<OpenMesh::HalfedgeHandle>& waveFront);

    void computeHomologyBasis();
    std::vector< std::vector< OpenMesh::HalfedgeHandle > > computeHomotopyBasis( OpenMesh::VertexHandle vertex );
    void computeSpirals();
    void computeSpiralPaths();

  public:

    // Get/Set methods
    PolyMesh& quadMesh();

    // Reduce all simple dual loops
    void reduceSimpleDualLoops();
    void reduceDualLoop(std::vector< OpenMesh::HalfedgeHandle > loop);

    void reduceCycle(QuadCycle cycle);

    void threeTwist( OpenMesh::VertexHandle vh );

    bool untangle();
    bool untangleBigon(Tangle tangle);

    void unspool( std::vector< OpenMesh::HalfedgeHandle > path);

  private:
    // Underlying mesh:
    PolyMesh& mesh_;

    // Reduce cycles
    void reduce2Cycle(QuadCycle cycle);
    void reduce4Cycle(QuadCycle cycle);
    void reduce6Cycle(QuadCycle cycle);

    void deleteCycleInterior(QuadCycle cycle);
    OpenMesh::VectorT<double,3> pointAverage( std::vector< OpenMesh::VertexHandle > vertices, bool reset);

    bool untangleBigonGeneric(Tangle tangle);
    bool untangleBigonAreaZero(Tangle tangle);

    // Replace the 1-ring of a vertex of valence 3 by 2 quads. The given
    // halfedge points from the vertex to the vertex of the 1-ring which
    // will be connected by an inner edge
    void quadReduction3to2(OpenMesh::HalfedgeHandle heh);

    // General modification code that should be available in general TODO
    // Two of these methods come from TriConnectivity.hh

    // Given a HEH, this method constructs a face with two edges in which the given
    // halfedge lies. The other halfedge of the face is returned
    OpenMesh::HalfedgeHandle insert_loop(OpenMesh::HalfedgeHandle _hh);

    // Given a "new" vertex and two halfedges pointing to the same vertex V,
    // this method splits the vertex V (it connects V and the "new" vertex).
    // During this, the angle defined by h0 and h1 is expanded to include
    // the new edge.
    OpenMesh::HalfedgeHandle insert_edge(OpenMesh::VertexHandle _vh, OpenMesh::HalfedgeHandle _h0, OpenMesh::HalfedgeHandle _h1);

    // Remove the given edge and combine the two adjacent faces.
    void vanish_edge(OpenMesh::EdgeHandle _eh);

    // Cut apart the two edges given by the path _h0 and _h1 and
    // insert a quad in between. The splitted vertex positions are
    // _pUp and _pDown, where _pUp lies in the direction of the path
    void extendQuad(OpenMesh::HalfedgeHandle _h0, OpenMesh::HalfedgeHandle _h1, OpenMesh::VectorT<double,3> _pUp, OpenMesh::VectorT<double,3> _pDown);
};



#endif // QUADMODIFIER_H
