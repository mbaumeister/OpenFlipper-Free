#ifndef QUADCYCLE_HH
#define QUADCYCLE_HH

#include <OpenFlipper/common/Types.hh>
#include <ObjectTypes/PolyMesh/PolyMesh.hh>
#include <OpenMesh/Core/Utils/Property.hh>

class QuadCycle
{
    public:
        QuadCycle( PolyMesh& _mesh, OpenMesh::VertexHandle _center);
        ~QuadCycle();

        // Access faces within the cycle
        std::vector< OpenMesh::FaceHandle > innerFaces();

        // Access boundary halfedges
        std::vector< OpenMesh::HalfedgeHandle > halfedgeBoundary();

        // Extend the cycle and return the number of added faces. Return -1 if an extension would violate the disc condition
        int extendCycle();

        // Categorise the different types of quad cycles, depending on their boundary
        enum CYCLE_TYPE {
            NONE,
            FOUR_ONE, FOUR_FLAT, // one = one square after reduction
            SIX_PINCH, SIX_ONE, SIX_FLAT, SIX_TWO, SIX_THREE // 1,2,3 = #squares after reduction, flat = reduce to flat line
        };
        CYCLE_TYPE getType();

        CYCLE_TYPE canonise(); // Analyse the QuadCycle and potentially update its cycle type. This also may change the boundary.

    private:
        // Underlying mesh
        std::reference_wrapper<PolyMesh> mesh_;

        // halfedges defining the cycle (they follow the 1-ring counter-clockwise, remaining on the inside)
        std::vector< OpenMesh::HalfedgeHandle > boundary_;

        // faces within the cycle
        std::vector< OpenMesh::FaceHandle > faces_;

        CYCLE_TYPE type;

        // Extend the cycle by the given face. Return true if successful and false if the disc condition would be violated
        bool extendCycle( OpenMesh::FaceHandle _face);

        void canonise_4();
        void canonise_6();
        long findPartner(OpenMesh::HalfedgeHandle heh, std::vector<OpenMesh::HalfedgeHandle> goal);
};

#endif // QUADCYCLE_HH
