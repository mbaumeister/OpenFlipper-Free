

//=============================================================================
//
//  CLASS QuadModificationPlugin - IMPLEMENTATION
//
//=============================================================================


//== INCLUDES =================================================================


#include "ModificationPlugin.hh"

//== IMPLEMENTATION ==========================================================

QuadModificationPlugin::QuadModificationPlugin() :
    tool_( nullptr ), toolIcon_( nullptr ), modifier_( nullptr ) {}

//-----------------------------------------------------------------------------

void QuadModificationPlugin::initializePlugin()
{
  if ( OpenFlipper::Options::gui() ) {
    tool_ = new ModificationToolbarWidget();
    QSize size(100, 100);
    tool_->resize(size);

    // connect signals->slots
    connect( tool_->pB_analyse, SIGNAL(clicked()), this, SLOT(slot_analyse()) ); // button: analyse
    connect( tool_->reduce_simple_loops, SIGNAL(clicked()), this, SLOT(slot_reduceSimpleLoops()));
    connect( tool_->reduce_selected_loop, SIGNAL(clicked()), this, SLOT(slot_reduceSelectedLoop()));
    connect( tool_->counter_dualLoops, QOverload<int>::of(&QSpinBox::valueChanged), this, &QuadModificationPlugin::slot_redraw );
    connect( tool_->counter_dualLoops, QOverload<int>::of(&QSpinBox::valueChanged), this, &QuadModificationPlugin::slot_dualLoopActivation );
    connect( tool_->cycle2_counter, QOverload<int>::of(&QSpinBox::valueChanged), this, &QuadModificationPlugin::slot_redraw );
    connect( tool_->cycle4_counter, QOverload<int>::of(&QSpinBox::valueChanged), this, &QuadModificationPlugin::slot_redraw );
    connect( tool_->cycle6_counter, QOverload<int>::of(&QSpinBox::valueChanged), this, &QuadModificationPlugin::slot_redraw );
    connect( tool_->cycle2_counter, QOverload<int>::of(&QSpinBox::valueChanged), this, &QuadModificationPlugin::slot_2buttonActivation );
    connect( tool_->cycle4_counter, QOverload<int>::of(&QSpinBox::valueChanged), this, &QuadModificationPlugin::slot_4buttonActivation );
    connect( tool_->cycle6_counter, QOverload<int>::of(&QSpinBox::valueChanged), this, &QuadModificationPlugin::slot_6buttonActivation );
    connect( tool_->reduce_cycle4_select, SIGNAL(clicked()), this, SLOT(slot_reduceSelected4cycle()) );
    connect( tool_->reduce_cycle6_select, SIGNAL(clicked()), this, SLOT(slot_reduceSelected6cycle()) );
    connect( tool_->tangle_counter, QOverload<int>::of(&QSpinBox::valueChanged), this, &QuadModificationPlugin::slot_redraw );
    connect( tool_->tangle_counter, QOverload<int>::of(&QSpinBox::valueChanged), this, &QuadModificationPlugin::slot_untangleButtonActivation );
    connect( tool_->untangleButton, SIGNAL(clicked()), this, SLOT(slot_untangle()));
    connect( tool_->untangleBigonButton, SIGNAL(clicked()), this, SLOT(slot_untangleBigon()));
    connect( tool_->spiral_counter, QOverload<int>::of(&QSpinBox::valueChanged), this, &QuadModificationPlugin::slot_redraw );
    connect( tool_->spiral_counter, QOverload<int>::of(&QSpinBox::valueChanged), this, &QuadModificationPlugin::slot_unspoolButtonActivation );
    connect( tool_->button_unspool, SIGNAL(clicked()), this, SLOT(slot_unspool()));
    connect( tool_->checkBox_BaseComplex, SIGNAL(clicked()), this, SLOT(slot_redraw) );

    toolIcon_ = new QIcon(OpenFlipper::Options::iconDirStr()+OpenFlipper::Options::dirSeparator()+"trefoil.png");
    emit addToolbox( tr("QuadModifier") , tool_, toolIcon_ );
  }
}

//-----------------------------------------------------------------------------

/** \brief Set the scripting slot descriptions
 *
 */
void QuadModificationPlugin::pluginsInitialized(){}


//-----------------------------------------------------------------------------

/** \brief Analyse a given quad mesh
 *
 */
void QuadModificationPlugin::slot_analyse()
{
  bool found = false;

  for ( PluginFunctions::ObjectIterator o_it(PluginFunctions::TARGET_OBJECTS,DATA_POLY_MESH) ;
        o_it != PluginFunctions::objectsEnd(); ++o_it)  {

    QString jobDescription = "Analysed (";

    PolyMeshObject* object = PluginFunctions::polyMeshObject(*o_it);

    // For the base complex (?)
    object->setObjectDrawMode(ACG::SceneGraph::DrawModes::SOLID_FACES_COLORED
                                  | ACG::SceneGraph::DrawModes::EDGES_COLORED);
    meshId_ = o_it->id();

    if ( object == nullptr ) {
      emit log(LOGWARN , "Unable to get object ( Only Quad Meshes supported)");
      continue;
    }

    found = true;


    // Get quad mesh
    PolyMesh* mesh = PluginFunctions::polyMesh(*o_it);

    if ( mesh == nullptr ) {
      emit log(LOGERR, "Unable to get mesh from object( Only Quad Meshes supported)");
      return;
    }

    // Create modifier
    modifier_ = new QuadModifier( *mesh);

    // Compute the number of faces
    long nrFaces = std::distance( mesh->faces_begin(), mesh->faces_end());
    tool_->value_nrQuads->setNum( int (nrFaces) );

    // Set the dual loops
    int nrDualLoops = int (mesh->property(modifier_->dualLoops_).size()) -1;
    resetSpinBox(tool_->counter_dualLoops, nrDualLoops);

    // Set the cycles
    int nr2cycles = int( mesh->property(modifier_->cycle2_).size());
    resetSpinBox(tool_->cycle2_counter, nr2cycles);

    int nr4cycles = int( mesh->property(modifier_->cycle4_).size());
    resetSpinBox(tool_->cycle4_counter, nr4cycles);

    int nr6cycles = int( mesh->property(modifier_->cycle6_).size());
    resetSpinBox(tool_->cycle6_counter, nr6cycles);


    tool_->reduce_cycle2_select->setEnabled(false);
    tool_->reduce_cycle2_all->setEnabled(false);
    tool_->reduce_cycle4_select->setEnabled(false);
    tool_->reduce_cycle4_all->setEnabled(false);
    tool_->reduce_cycle6_select->setEnabled(false);
    tool_->reduce_selected_loop->setEnabled(false);

    int nrTangles = int( mesh->property(modifier_->tangles_).size() );
    resetSpinBox(tool_->tangle_counter, nrTangles);

    int nrSpirals = int (mesh->property(modifier_->spiralPaths_).size());
    resetSpinBox(tool_->spiral_counter, nrSpirals);

    // Enable generic buttons
    tool_->reduce_simple_loops->setEnabled(true);
    tool_->untangleButton->setEnabled(true);

    slot_redraw();
  }

  if ( !found )
    emit log(LOGERR , tr("Unable to analyse. No quad mesh selected as target!") );

}

QString QuadModificationPlugin::ofN(int n)
{
    QString str;
    str.setNum(n);
    str.prepend(QString("/"));
    return str;
}

void QuadModificationPlugin::resetSpinBox(QSpinBox* box, int max)
{
    bool oldState = box->blockSignals(true);
    box->setValue(0);
    box->setMaximum( max );
    box->setSuffix(ofN(max));
    box->blockSignals(oldState);
}

void QuadModificationPlugin::slot_dualLoopActivation(int value)
{
    if (value == 0){
        tool_->reduce_selected_loop->setEnabled(false);
    }
    else {
        tool_->reduce_selected_loop->setEnabled(true);
    }
}

void QuadModificationPlugin::slot_2buttonActivation(int value)
{
    if (value == 0){
        tool_->reduce_cycle2_select->setEnabled(false);
    }
    else {
        tool_->reduce_cycle2_select->setEnabled(true);
    }
}

void QuadModificationPlugin::slot_4buttonActivation(int value)
{
    if (value == 0){
        tool_->reduce_cycle4_select->setEnabled(false);
    }
    else {
        tool_->reduce_cycle4_select->setEnabled(true);
    }
}

void QuadModificationPlugin::slot_6buttonActivation(int value)
{
    if (value == 0){
        tool_->reduce_cycle6_select->setEnabled(false);
        tool_->cycle6_volume->setText("-");
    }
    else {
        tool_->reduce_cycle6_select->setEnabled(true);
        // Compute the volume (if applicable)
        QuadCycle cycle = modifier_->quadMesh().property(modifier_->cycle6_)[value-1];
        cycle.canonise();
        if( cycle.innerFaces().size() == 6 && cycle.getType() == QuadCycle::SIX_TWO ) {
            // compute volume
            // TODO
        } else {
            tool_->cycle6_volume->setText("-");
        }
    }
}

Tangle QuadModificationPlugin::getTangle(int index)
{
    std::set< Tangle > tangles = modifier_->quadMesh().property(modifier_->tangles_);
    std::set< Tangle >::iterator it = tangles.begin();
    std::advance(it, index);
    return *it;
}

void QuadModificationPlugin::slot_untangleButtonActivation(int value)
{
    if (value == 0){
        tool_->untangleBigonButton->setEnabled(false);
    }
    else {
        tool_->untangleBigonButton->setEnabled(true);
        Tangle tangle = getTangle(value - 1);

        OpenMesh::HalfedgeHandle first = tangle.boundary_[0];
        OpenMesh::VertexHandle vertex = modifier_->quadMesh().from_vertex_handle(first);
        auto point = modifier_->quadMesh().point( vertex );
        auto normal = modifier_->quadMesh().normal( vertex );
        //PluginFunctions::flyTo(point + normal, point, 100.0);
    }
}

void QuadModificationPlugin::slot_unspoolButtonActivation(int value)
{
    if (value == 0){
        tool_->button_unspool->setEnabled(false);
    }
    else {
        tool_->button_unspool->setEnabled(true);
    }
}

void QuadModificationPlugin::slot_reduceSelected4cycle()
{
    // Find out which 4-cycle is selected
    int index = tool_->cycle4_counter->value();
    QuadCycle cycle = modifier_->quadMesh().property(modifier_->cycle4_)[index-1];
    modifier_->reduceCycle(cycle);
    update(UPDATE_GEOMETRY);
}

void QuadModificationPlugin::slot_reduceSelected6cycle()
{
    // Find out which 6-cycle is selected
    int index = tool_->cycle6_counter->value();
    QuadCycle cycle = modifier_->quadMesh().property(modifier_->cycle6_)[index-1];
    modifier_->reduceCycle(cycle);
    update(UPDATE_GEOMETRY);
}

void QuadModificationPlugin::slot_reduceSimpleLoops()
{
    modifier_->reduceSimpleDualLoops();
    update(UPDATE_GEOMETRY);
}

void QuadModificationPlugin::update(const UpdateType update)
{
    modifier_->quadMesh().garbage_collection();
    modifier_->quadMesh().update_normals();
    emit updatedObject(meshId_, update);
    slot_analyse();
}

void QuadModificationPlugin::slot_reduceSelectedLoop()
{
    // Find out which loop is selected
    int index = tool_->counter_dualLoops->value();
    std::vector< OpenMesh::HalfedgeHandle > loop = modifier_->quadMesh().property(modifier_->dualLoops_)[index];
    modifier_->reduceDualLoop(loop);
    update(UPDATE_GEOMETRY);
}

void QuadModificationPlugin::slot_redraw()
{
    draw_.clear();
    PolyMesh& mesh = modifier_->quadMesh();
    for( auto heh = mesh.halfedges_begin(); heh != mesh.halfedges_end(); heh++) {
        mesh.status(*heh).set_selected(false);
    }

    if( tool_->checkBox_BaseComplex->isChecked()) {
         bool is_quad = true;
         for (auto fh : mesh.faces()) {
            is_quad &= fh.valence() == 4;
            if (!is_quad) {
                continue;
            }
         }

         show_base_complex(mesh, 4, true, 1.0, IEM_Alpha_1, 250);
    }

    // Draw the dual loop first
    int index = tool_->counter_dualLoops->value();
    if (index > 0){
        std::vector< OpenMesh::HalfedgeHandle > loop = mesh.property(modifier_->dualLoops_)[index];

        unsigned long mod = loop.size();
        for (unsigned long i = 0; i < loop.size(); i ++){
            auto from = mesh.calc_edge_midpoint(loop[i]);
            auto to = mesh.calc_edge_midpoint(loop[(i+1)%mod]);
            draw_.line( from, to, Draw::Color(0,0,1,1));
        }
    }

    // Draw the cycles
    int index2 = tool_->cycle2_counter->value();
    if (index2 > 0){
        QuadCycle cycle = mesh.property(modifier_->cycle2_)[index2-1];
        drawQuadCycle(cycle, Draw::Color(1,0,0,1));
    }
    int index4 = tool_->cycle4_counter->value();
    if (index4 > 0){
        QuadCycle cycle = mesh.property(modifier_->cycle4_)[index4-1];
        drawQuadCycle(cycle, Draw::Color(0,1,0,1));
    }
    int index6 = tool_->cycle6_counter->value();
    if (index6 > 0){
        QuadCycle cycle = mesh.property(modifier_->cycle6_)[index6-1];
        drawQuadCycle(cycle, Draw::Color(1,0,1,1));
    }

    // Draw the tangle
    int indexT = tool_->tangle_counter->value();
    if (indexT > 0){
        std::set< Tangle > tangles = mesh.property(modifier_->tangles_);
        std::set< Tangle >::iterator it = tangles.begin();
        std::advance(it, indexT-1);
        Tangle tangle = *it;
        tool_->tangle_area->setNum( int(tangle.area_) );
        drawTangle(tangle);
    } else {
        tool_->tangle_area->setNum( -1 );
    }

    // Draw the spiral
    int indexS = tool_->spiral_counter->value();
    if (indexS > 0 ){
        std::vector< OpenMesh::HalfedgeHandle > spiral = mesh.property(modifier_->spiralPaths_)[indexS-1];
        drawSpiral(spiral, Draw::Color(0,1,1,1));
    }

    emit updatedObject(meshId_, UPDATE_GEOMETRY);
    emit updateView();
}

void QuadModificationPlugin::drawQuadCycle(QuadCycle cycle, Draw::Color col)
{
    PolyMesh& mesh = modifier_->quadMesh();
    for (auto heh : cycle.halfedgeBoundary())
    {
        auto to = mesh.to_vertex_handle(heh);
        auto from = mesh.from_vertex_handle(heh);
        draw_.line( mesh.point(from), mesh.point(to), col);
    }
}

void QuadModificationPlugin::drawTangle(Tangle t)
{
    PolyMesh& mesh = modifier_->quadMesh();
    if (!mesh.has_halfedge_status()) {
        mesh.request_halfedge_status();
    }
    for( OpenMesh::HalfedgeHandle heh : t.boundary_) {
        mesh.status(heh).set_selected(true);
    }
}

void QuadModificationPlugin::drawSpiral(std::vector<OpenMesh::HalfedgeHandle> spiral, Draw::Color col)
{
    PolyMesh& mesh = modifier_->quadMesh();
    for (OpenMesh::HalfedgeHandle heh : spiral ) {
        auto to = mesh.to_vertex_handle(heh);
        auto from = mesh.from_vertex_handle(heh);
        draw_.line( mesh.point(from), mesh.point(to), col);
    }
}

void QuadModificationPlugin::slot_untangle()
{
    modifier_->untangle();
    update(UPDATE_ALL); // Since we possibly added vertices and change the underlying graph
}

void QuadModificationPlugin::slot_untangleBigon()
{
    // Find out which bigon is selected
    int index = tool_->tangle_counter->value();
    std::set< Tangle > tangles = modifier_->quadMesh().property(modifier_->tangles_);
    std::set< Tangle >::iterator it = tangles.begin();
    std::advance(it, index-1);
    Tangle tangle = *it;

    modifier_->untangleBigon(tangle);
    update(UPDATE_ALL);
}

void QuadModificationPlugin::slot_unspool()
{
    // Find out which spiral is selected
    int index = tool_->spiral_counter->value();
    auto path = modifier_->quadMesh().property( modifier_->spiralPaths_ )[index-1];
    modifier_->unspool(path);
    update(UPDATE_ALL);
}

//-----------------------------------------------------------------------------

bool QuadModificationPlugin::is_regular(PolyMesh& _mesh, const PolyMesh::VertexHandle& _vh, const unsigned int _val_reg, const unsigned int _val_reg_bound)
{
   if( _mesh.is_boundary(_vh) )
     return ( _mesh.valence(_vh) == _val_reg_bound);
   else
     return ( _mesh.valence(_vh) == _val_reg);
}

void QuadModificationPlugin::show_base_complex(PolyMesh& _mesh, int _valence_regular, bool colorEdges, float edgebrightness, INNER_EDGE_MODE innerEdgeMode, int halton_skip)
{
   // request edge and face colors
   if (colorEdges)
     _mesh.request_edge_colors();
   _mesh.request_face_colors();

   // triangle mesh setting is default
   int val_reg     = 6;
   int val_reg_bnd = 4;
   int n_rot_cw    = 2;

   // quad (poly) mesh?
   if(_valence_regular != 6)
   {
     val_reg     = 4;
     val_reg_bnd = 3;
     n_rot_cw    = 1;
   }

   // reset edge tags
   PolyMesh::EdgeIter e_it  = _mesh.edges_begin();
   PolyMesh::EdgeIter e_end = _mesh.edges_end();
   for(; e_it != e_end; ++e_it)
     _mesh.status(*e_it).set_tagged(false);

   int n_bc_vertices = 0;
   int n_bc_reg_vertices = 0;

   // tag BC-Edges
   PolyMesh::VertexIter v_it  = _mesh.vertices_begin();
   PolyMesh::VertexIter v_end = _mesh.vertices_end();
   for(; v_it != v_end; ++v_it)
   {
     // irregular vertex?
     if(!is_regular(_mesh, *v_it, val_reg, val_reg_bnd))
     {
//      std::cerr << "boundary: " << int(_mesh.is_boundary(v_it.handle())) << " valence: " << _mesh.valence(v_it.handle()) << std::endl;

       ++ n_bc_vertices;

       //  iterate over all outgoing halfedges which are not boundary and not tagged
       PolyMesh::VOHIter voh_it = _mesh.voh_iter(*v_it);
       for(; voh_it.is_valid(); ++voh_it)
         if(!_mesh.is_boundary(*voh_it) &&
            !_mesh.status(_mesh.edge_handle(*voh_it)).tagged())
           {
             PolyMesh::HalfedgeHandle  heh_cur = *voh_it;

             bool started_at_boundary = _mesh.is_boundary(_mesh.edge_handle(heh_cur));

             while(1)
             {
               // tag
               _mesh.status(_mesh.edge_handle(heh_cur)).set_tagged(true);

               // hit boundary?
               if(!started_at_boundary && _mesh.is_boundary(_mesh.to_vertex_handle(heh_cur)))
                   break;

               // move to next
               if(is_regular(_mesh, _mesh.to_vertex_handle(heh_cur), val_reg, val_reg_bnd))
               {
                 heh_cur = _mesh.next_halfedge_handle(heh_cur);
                 if(_mesh.status(_mesh.edge_handle(heh_cur)).tagged()) n_bc_reg_vertices++;
                 for(int i=0; i<n_rot_cw; ++i)
                   heh_cur = _mesh.cw_rotated_halfedge_handle(heh_cur);
               }
               else break; //
             }
           }
     }
   }

   // colorize patches
   int n_bc_faces(0);
   // allocate memory for patch_idx
   std::vector<bool> face_colored(_mesh.n_faces(),false);
   // ColorGenerator
//  ACG::HuePartitioningColors cg;
   ACG::HaltonColors hcol(halton_skip);

//  hcol.reset(halton_skip);

   PolyMesh::FaceIter f_it  = _mesh.faces_begin();
   PolyMesh::FaceIter f_end = _mesh.faces_end();
   for(; f_it != f_end; ++f_it)
   {
     // uncolored face?
     if( !face_colored[f_it->idx()])
     {
       ++n_bc_faces;
       // get new color
//      Vec4f col = cg.generateNextColor();
       ACG::Vec4f col = hcol.get_next_color();

       std::queue<PolyMesh::FaceHandle> fq;
       fq.push(*f_it);
       while(!fq.empty())
       {
         PolyMesh::FaceHandle fh = fq.front();
         fq.pop();

         // already colored?
         if( face_colored[fh.idx()]) continue;

         _mesh.set_color(fh, col);
         face_colored[fh.idx()] = true;

         // queue neighbors if possible
         PolyMesh::FHIter fh_iter = _mesh.fh_iter(fh);
         for(; fh_iter.is_valid(); ++fh_iter)
           if( !_mesh.status(_mesh.edge_handle(*fh_iter)).tagged())
           {
             PolyMesh::HalfedgeHandle heh_opp = _mesh.opposite_halfedge_handle(*fh_iter);
             if(!_mesh.is_boundary(heh_opp))
               fq.push(_mesh.face_handle(heh_opp));
           }
       }

     }
   }

   if (colorEdges) {
   // colorize BC-Edges
    e_it  = _mesh.edges_begin();
    e_end = _mesh.edges_end();
    for(; e_it != e_end; ++e_it)
    {
      if(_mesh.status(*e_it).tagged()) {
          _mesh.set_color(*e_it, PolyMesh::Color(0,0,0,1));
      } else {
        PolyMesh::HalfedgeHandle h0 = _mesh.halfedge_handle(*e_it,0);
        PolyMesh::FaceHandle f0 = _mesh.face_handle(h0);
        PolyMesh::Color c = _mesh.color(f0);
        float alpha;
        switch (innerEdgeMode) {
            case IEM_Alpha_0:
                alpha = 0.0;
                break;
            case IEM_Alpha_Keep:
                alpha = _mesh.color(*e_it)[3];
                break;
            case IEM_Alpha_1:
            default:
                alpha = 1.0;
                break;
        }
        _mesh.set_color(*e_it, PolyMesh::Color(c[0]*edgebrightness,c[1]*edgebrightness,c[2]*edgebrightness, alpha));
      }
    }
   }


   std::cerr << "#BC-Vertices: " << n_bc_vertices << " + " << n_bc_reg_vertices << " = " << n_bc_vertices+n_bc_reg_vertices << std::endl;
   std::cerr << "#BC-Faces   : " << n_bc_faces     << std::endl;
}
