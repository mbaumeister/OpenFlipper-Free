#include "QuadCycle.hh"

// Cycle code
QuadCycle::QuadCycle(PolyMesh& _mesh, OpenMesh::VertexHandle _center) : mesh_(_mesh), type(QuadCycle::NONE)
{
    std::vector< OpenMesh::HalfedgeHandle > bound;
    std::vector< OpenMesh::FaceHandle > faces;
    bound.clear();
    faces.clear();
    for (auto heIter=_mesh.voh_ccwiter(_center); heIter != _mesh.voh_ccwend(_center); ++heIter )
    {
        OpenMesh::HalfedgeHandle next = _mesh.next_halfedge_handle(*heIter);
        bound.push_back( next );
        bound.push_back( _mesh.next_halfedge_handle(next));
        faces.push_back( _mesh.face_handle(*heIter));
    }
    boundary_ = bound;
    faces_ = faces;
}


QuadCycle::~QuadCycle(){}

std::vector< OpenMesh::FaceHandle > QuadCycle::innerFaces()
{
    return faces_;
}

std::vector< OpenMesh::HalfedgeHandle > QuadCycle::halfedgeBoundary()
{
    return boundary_;
}

int QuadCycle::extendCycle()
{
    int howManyTimes = 0;
    bool checked = false;
    PolyMesh& mesh = mesh_;
    while(!checked)
    {
        checked = true;

        // Find a position to extend
        OpenMesh::FaceHandle face;
        for (unsigned int i = 0; i < boundary_.size(); ++i)
        {
            OpenMesh::FaceHandle f1 = mesh.opposite_face_handle(boundary_[i]);
            OpenMesh::FaceHandle f2 = mesh.opposite_face_handle(boundary_[(i+1) % boundary_.size()]);
            if (f1 == f2){
                face = f1;
                checked = false;
                break;
            }
        }

        if (!checked) {
            if (extendCycle(face)){
                ++howManyTimes;
            } else {
                return -1;
            }
        }
    }
    return howManyTimes;
}

bool QuadCycle::extendCycle(OpenMesh::FaceHandle _face)
{
    std::vector< unsigned int > positions;
    PolyMesh& mesh = mesh_;
    for (unsigned int i = 0; i < boundary_.size(); ++i)
    {
        OpenMesh::HalfedgeHandle heh = mesh.opposite_halfedge_handle( boundary_[i]);
        // Check whether heh is a halfedge-handle of _face
        if (mesh.face_handle(heh) == _face)
        {
            positions.push_back(i);
        }
    }

    if( positions.size() == 2){
        unsigned int first, second;
        if (positions[0]+1 == positions[1]) {
            first = positions[0];
            second = positions[1];
        } else {
            first = positions[1];
            second = positions[0];
        }

        auto sm_first = OpenMesh::make_smart( boundary_[first], mesh_ );
        auto sm_second = OpenMesh::make_smart( boundary_[second], mesh_ );

        // Compute the other vertex of _face
        OpenMesh::VertexHandle otherVertex = sm_first.opp().next().to();
        // Check whether this new vertex is already part of the cycle
        bool found = false;
        for (auto heh : boundary_){
            if (mesh.to_vertex_handle(heh) == otherVertex){
                found = true;
                break;
            }
        }
        if (found){
            return false;
        }
        // Otherwise we add the face
        boundary_[first] = sm_first.opp().next();
        boundary_[second] = sm_second.opp().prev();
        faces_.push_back(_face);
        return true;
    } else if(positions.size() == 3){
        // There are three possibilities (L is length of boundary, n an arbitrary position):
        // [n,n+1,n+2]
        // [L-2,L-1,0]
        // [L-1,0,1]
        if (positions[0]==0 && positions[1] != 1) {
            auto smart = OpenMesh::make_smart( boundary_[0], mesh_);
            boundary_[0] = smart.opp().prev();
            boundary_.pop_back();
            boundary_.pop_back();
        } else if (positions[0]==0 && positions[2] != 2) {
            auto smart = OpenMesh::make_smart( boundary_[1], mesh_);
            boundary_.pop_back();
            boundary_.push_back(smart.opp().prev());
            boundary_.erase( boundary_.begin(), boundary_.begin()+2);
        } else {
            auto smart = OpenMesh::make_smart( boundary_[positions[0]], mesh_);
            boundary_[positions[2]] = smart.opp().next();
            boundary_.erase( boundary_.begin()+positions[0], boundary_.begin()+positions[0]+2);
        }
        faces_.push_back(_face);
        return true;
    } else {
        return false;
    }
}

QuadCycle::CYCLE_TYPE QuadCycle::getType()
{
    return type;
}

QuadCycle::CYCLE_TYPE QuadCycle::canonise()
{
    if( type != NONE) {
        return type;
    }

    if( boundary_.size() == 4 ) {
        canonise_4();
    } else if ( boundary_.size() == 6 ) {
        canonise_6();
    }

    return type;
}

long QuadCycle::findPartner(OpenMesh::HalfedgeHandle heh, std::vector<OpenMesh::HalfedgeHandle> goal)
{
    PolyMesh& mesh = mesh_;
    auto pos = goal.end();
    auto check = mesh.opposite_halfedge_handle(heh);
    while( pos == goal.end() ){
        check = mesh.next_halfedge_handle( mesh.next_halfedge_handle( mesh.opposite_halfedge_handle( check ) ) );
        pos = std::find( goal.begin(), goal.end(), check);
    }
    return std::distance( goal.begin(), pos );
}

void QuadCycle::canonise_4()
{
    long index = findPartner(boundary_[0], boundary_);
    if( index == 2 ) {
        type = FOUR_ONE;
        return;
    }

    // Otherwise we are in the flat case
    // We rearrange the boundary such that indices 0 and 1 are partners
    if( index == 3) {
        OpenMesh::HalfedgeHandle start = boundary_[0];
        boundary_.erase(boundary_.begin());
        boundary_.push_back(start);
    }
    type = FOUR_FLAT;
}

void QuadCycle::canonise_6()
{
    std::vector<long> permutation(6,-1);
    for (unsigned int i = 0; i < 6; i++) {
        if (permutation[i] == -1) {
            long partner = findPartner(boundary_[i], boundary_);
            permutation[i] = partner;
            permutation[partner] = i;
        }
    }

    /*
     * There are 15 different possibilities for the form
     * of this permutation, split into 5 types.
     * SIX_PINCH:
     *      103254
     *      521430
     * SIX_FLAT:
     *      105432
     *      543210
     *      321054
     * SIX_THREE:
     *      345012
     * SIX_ONE:
     *      104523
     *      421503
     *      453201
     *      250431
     *      230154
     *      534120
     * SIX_TWO:
     *      354021
     *      240513
     *      435102
     * The first three numbers determine the permutation
     * uniquely. Thus, we use these numbers to determine
     * the type and shift (the rotation to construct a
     * standard form of each type).
     */
    int shift(-1);
    switch (permutation[0]) {
        case 1:
            switch (permutation[2]) {
                case 3: type = SIX_PINCH; shift = 0; break;
                case 4: type = SIX_ONE; shift = 0; break;
                case 5: type = SIX_FLAT; shift = 0; break;
            }
            break;
        case 2:
            switch (permutation[1]) {
                case 3: type = SIX_ONE; shift = 4; break;
                case 4: type = SIX_TWO; shift = 1; break;
                case 5: type = SIX_ONE; shift = 3; break;
            }
            break;
        case 3:
            switch (permutation[1]) {
                case 2: type = SIX_FLAT; shift = 1; break;
                case 4: type = SIX_THREE; shift = 0; break;
                case 5: type = SIX_TWO; shift = 0; break;
            }
            break;
        case 4:
            switch( permutation[1]){
                case 2: type = SIX_ONE; shift = 1; break;
                case 3: type = SIX_TWO; shift = 2; break;
                case 5: type = SIX_ONE; shift = 2; break;
            }
            break;
        case 5:
            switch( permutation[1]) {
                case 2: type = SIX_PINCH; shift = 1; break;
                case 3: type = SIX_ONE; shift = 5; break;
                case 4: type = SIX_FLAT; shift = 2; break;
            }
            break;
    }
    if (type == NONE or shift == -1) {
        throw std::runtime_error("Cycle type cannot be determined.");
    }

    // shift the boundary
    std::rotate(boundary_.begin(), boundary_.begin()+shift, boundary_.end());
}
