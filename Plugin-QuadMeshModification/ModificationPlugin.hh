//=============================================================================
//
// ModificationPlugin
//
//=============================================================================

#pragma once

//== INCLUDES =================================================================

#include <QObject>
#include <QMenuBar>
#include <QTimer>

#include <OpenFlipper/BasePlugin/BaseInterface.hh>
#include <OpenFlipper/BasePlugin/ToolboxInterface.hh>
#include <OpenFlipper/BasePlugin/BackupInterface.hh>
#include <OpenFlipper/BasePlugin/ScriptInterface.hh>
#include <OpenFlipper/BasePlugin/LoggingInterface.hh>
#include <OpenFlipper/BasePlugin/PythonInterface.hh>

#include <OpenFlipper/common/Types.hh>
#include <ObjectTypes/PolyMesh/PolyMesh.hh>
#include <ObjectTypes/TriangleMesh/TriangleMesh.hh>

#include <ACG/Utils/HaltonColors.hh>
#include <queue>

#include "ModificationToolbarWidget.hh"
#include "QuadModifier.hh"
#include "Draw.hh"


//== CLASS DEFINITION =========================================================


/** Plugin for Quad Mesh Modification Support
 */
class QuadModificationPlugin : public QObject, BaseInterface, ToolboxInterface, LoggingInterface, BackupInterface, ScriptInterface, PythonInterface
{
  Q_OBJECT
  Q_INTERFACES(BaseInterface)
  Q_INTERFACES(ToolboxInterface)
  Q_INTERFACES(BackupInterface)
  Q_INTERFACES(LoggingInterface)
  Q_INTERFACES(ScriptInterface)
  Q_INTERFACES(PythonInterface)

  Q_PLUGIN_METADATA(IID "org.OpenFlipper.Plugins.Plugin-QuadMeshModification")

signals:

  // BaseInterface
  void updateView();

  void updatedObject(int, const UpdateType&);

  void setSlotDescription(QString     _slotName,   QString     _slotDescription,
                          QStringList _parameters, QStringList _descriptions);

  // LoggingInterface
  void log(Logtype _type, QString _message);
  void log(QString _message);

  // ToolboxInterface
  void addToolbox( QString _name  , QWidget* _widget, QIcon* _icon );

  // BackupInterface
  void createBackup( int _id , QString _name, UpdateType _type = UPDATE_ALL );

  // ScriptInterface
  void scriptInfo(QString _functionName);

public :

  /// default constructor
  QuadModificationPlugin();

  /// default destructor
  ~QuadModificationPlugin() {}

  /// Name of the Plugin
  QString name(){ return (QString("QuadMeshModification")); }

  /// Description of the Plugin
  QString description() { return (QString("Modifies Quad Meshes")); }


private:

  void initializePlugin(); // BaseInterface

  /// Second initialization stage
  void pluginsInitialized();

  /// Widget for Toolbox
  ModificationToolbarWidget* tool_;
  QIcon* toolIcon_;
  QuadModifier* modifier_;
  Draw draw_;
  int meshId_;

  enum INNER_EDGE_MODE {
           IEM_Alpha_1, IEM_Alpha_0, IEM_Alpha_Keep
       };

   bool is_regular(PolyMesh& _mesh, const PolyMesh::VertexHandle& _vh, const unsigned int _val_reg, const unsigned int _val_reg_bound);
   void show_base_complex(PolyMesh& _mesh, int _valence_regular, bool colorEdges, float edgebrightness, INNER_EDGE_MODE innerEdgeMode, int halton_skip);


private slots:

    /// Slot connected to the analyse button in the toolbox
    void slot_analyse();
    /// Slot for visualisation
    void slot_redraw();
    /// Slots to (de)activate cycle-contraction buttons
    void slot_dualLoopActivation(int value);
    void slot_2buttonActivation(int value);
    void slot_4buttonActivation(int value);
    void slot_6buttonActivation(int value);
    /// Slots to reduce individual cycles
    void slot_reduceSelected2cycle();
    void slot_reduceSelected4cycle();
    void slot_reduceSelected6cycle();

    void slot_reduceSimpleLoops();
    void slot_reduceSelectedLoop();

    void slot_untangle();
    void slot_untangleBigon();
    void slot_untangleButtonActivation(int value);
    Tangle getTangle(int index);

    void slot_unspoolButtonActivation(int value);
    void slot_unspool();

    void update(const UpdateType update);

//===========================================================================
/** @name Scripting Functions
  * @{ */
//===========================================================================
public slots:

   /** @} */

public slots:
   QString version() { return QString("1.0"); }



private:
    void drawQuadCycle(QuadCycle cycle, Draw::Color col);
    QString ofN(int n);
    void drawTangle(Tangle t);
    void drawSpiral( std::vector< OpenMesh::HalfedgeHandle > spiral, Draw::Color col);
    void resetSpinBox(QSpinBox* box, int max);
};
