#include "QuadModifier.hh"
#include <ObjectTypes/PolyMesh/PolyMesh.hh>
#include <ObjectTypes/TriangleMesh/TriangleMesh.hh>


// Tangle code
bool Tangle::operator==(const Tangle &t)
{
    if (this->boundary_.size() != t.boundary_.size()) {
        return false;
    }
    for (unsigned int i = 0; i < this->boundary_.size(); i++) {
        if (this->boundary_[i] != t.boundary_[i]) {
            return false;
        }
    }
    return true;
}

bool Tangle::operator<(const Tangle &t) const
{
    if (this->area_ != t.area_) {
        return this->area_ < t.area_;
    }
    if (this->boundary_.size() != t.boundary_.size() ){
        return this->boundary_.size() < t.boundary_.size();
    }
    return this->boundary_ < t.boundary_;
}

Tangle Tangle::errorTangle()
{
    Tangle t;
    t.success_ = false;
    return t;
}


// Modification code
QuadModifier::QuadModifier(PolyMesh& _mesh) : mesh_(_mesh)
{
    mesh_.add_property( dualLoops_ );
    mesh_.add_property( dualLoopIndex_);
    mesh_.add_property( cycle2_ );
    mesh_.add_property( cycle4_ );
    mesh_.add_property( cycle6_ );
    mesh_.add_property( tangles_);
    mesh_.add_property( homologyBasis_ );
    mesh_.add_property( spirals_ );
    mesh_.add_property( spiralPaths_ );
    analyseMesh();
}

QuadModifier::~QuadModifier()
{
    mesh_.remove_property( dualLoops_ );
    mesh_.remove_property( dualLoopIndex_ );
    mesh_.remove_property( cycle2_ );
    mesh_.remove_property( cycle4_ );
    mesh_.remove_property( cycle6_ );
    mesh_.remove_property( tangles_);
    mesh_.remove_property( homologyBasis_ );
    mesh_.remove_property( spirals_ );
    mesh_.remove_property( spiralPaths_ );
}

void QuadModifier::analyseMesh()
{
    computeDualLoops();
    computeTangles();
    computeCycles();
    computeHomologyBasis();
    computeSpirals();
    computeSpiralPaths();
}

PolyMesh& QuadModifier::quadMesh()
{
    return mesh_;
}

void QuadModifier::computeDualLoops()
{
    std::vector< std::vector< OpenMesh::HalfedgeHandle > > dualLoops;
    std::vector< OpenMesh::HalfedgeHandle > empty;
    empty.clear();
    dualLoops.push_back( empty );

    // Set all loop indices to 0
    for (auto heh : mesh_.halfedges())
    {
        mesh_.property(dualLoopIndex_, heh) = 0;
    }

    for (auto heh : mesh_.halfedges())
    {
        if (mesh_.property(dualLoopIndex_, heh) == 0)
        {
            int index = int ( dualLoops.size() );
            // Construct a new loop
            std::vector< OpenMesh::HalfedgeHandle > loop;
            loop.push_back(heh);
            mesh_.property(dualLoopIndex_, heh) = index;
            mesh_.property(dualLoopIndex_, heh.opp()) = -index;
            while (loop[0] != OpenMesh::make_smart(loop[loop.size()-1],mesh_).next().next().opp() )
            {
                OpenMesh::HalfedgeHandle current = loop[loop.size()-1];
                auto scurrent = OpenMesh::make_smart(current, mesh_);
                mesh_.property(dualLoopIndex_, scurrent.next().next()) = -index;
                mesh_.property(dualLoopIndex_, scurrent.next().next().opp()) = index;
                loop.push_back( scurrent.next().next().opp());
            }
            dualLoops.push_back(loop);
        }
    }
    mesh_.property( dualLoops_ ) = dualLoops;
}

void QuadModifier::computeTangles()
{
    std::set< Tangle > tangles;
    for (OpenMesh::FaceHandle fh : mesh_.faces() ) {
        // Find all halfedges of the face fh
        std::vector< OpenMesh::HalfedgeHandle > incidentHalfedges;
        for( auto iter = mesh_.fh_begin(fh); iter != mesh_.fh_end(fh); iter++ ) {
            incidentHalfedges.push_back(*iter);
        }

        for (unsigned int i = 0; i < 4; i++){
            OpenMesh::HalfedgeHandle first = incidentHalfedges[i];
            OpenMesh::HalfedgeHandle second = incidentHalfedges[(i+1)%4];

            Tangle t = growTangle(first, second);
            if (t.success_) {
                tangles.insert(t);
            }
        }
    }
    mesh_.property( tangles_ ) = tangles;
}

Tangle QuadModifier::growTangle(OpenMesh::HalfedgeHandle first, OpenMesh::HalfedgeHandle second)
{
    // Set up the minimal and maximal intersection positions (assuming no degree-2-vertices)
    unsigned long minIntersect = 2;
    unsigned long maxIntersect = std::max(
        mesh_.property(dualLoops_)[std::abs(mesh_.property(dualLoopIndex_, first))].size(),
        mesh_.property(dualLoops_)[std::abs(mesh_.property(dualLoopIndex_, second))].size());

    // The lists of halfedge-paths along the two edges of the 2-gon (pointing outwards)
    std::vector< OpenMesh::HalfedgeHandle > up, down;
    up.push_back( OpenMesh::make_smart(second, mesh_).opp().next() );
    down.push_back( OpenMesh::make_smart(first, mesh_).opp().prev() );

    // Initialise the wave front and boundary faces (the wave front points in its flow direction)
    std::set< OpenMesh::FaceHandle > boundaryFaces;
    boundaryFaces.insert( mesh_.face_handle(first) );
    boundaryFaces.insert( mesh_.face_handle(up[0]));
    boundaryFaces.insert( mesh_.face_handle(down[0]));

    std::vector< OpenMesh::HalfedgeHandle > waveFront;
    waveFront.push_back( mesh_.opposite_halfedge_handle(up[0]));
    waveFront.push_back( mesh_.opposite_halfedge_handle(down[0]));
    waveCollapse(waveFront);
    std::vector< OpenMesh::FaceHandle > submergedFaces;

    if (!flood(waveFront, submergedFaces, mesh_.from_vertex_handle(waveFront[0]))) {
        return Tangle::errorTangle();
    }
    minIntersect = std::max( minIntersect, submergedFaces.size()+1);
    if (minIntersect > maxIntersect) {
        return Tangle::errorTangle();
    }

    // Grow 2-gon and wave front simultaneously
    for (unsigned long l = 2; l < maxIntersect; l++) {
        // Construct the next elements along the 2-gon
        auto newUp = OpenMesh::make_smart(up[l-2],mesh_).next().opp().next();
        auto newDown = OpenMesh::make_smart(down[l-2],mesh_).prev().opp().prev();

        // If one of these faces appeared before, we will not find a tangle
        if( boundaryFaces.count(newUp.face()) + boundaryFaces.count(newDown.face()) ) {
            return Tangle::errorTangle();
        }
        boundaryFaces.insert(newUp.face());
        boundaryFaces.insert(newDown.face());

        // Check whether the 2-gon closes
        if (newUp.face() == newDown.face()) {
            if (minIntersect > l or waveFront.size() > 0) {
                return Tangle::errorTangle();
            }
            // Construct the tangle
            std::reverse( down.begin(), down.end());
            std::vector< OpenMesh::HalfedgeHandle > tangleBoundary;
            if (up[0] < down[0]) {
                // First up, then down
                tangleBoundary.insert( tangleBoundary.end(), up.begin(), up.end());
                tangleBoundary.insert( tangleBoundary.end(), down.begin(), down.end());
            } else {
                // First down, then up
                tangleBoundary.insert( tangleBoundary.end(), down.begin(), down.end());
                tangleBoundary.insert( tangleBoundary.end(), up.begin(), up.end());
            }
            Tangle result;
            result.success_ = true;
            result.area_ = submergedFaces.size();
            result.boundary_ = tangleBoundary;
            return result;
        } else {
            // Update the wave front
            waveFront.insert(waveFront.begin(), newUp.opp());
            waveFront.push_back(newDown.opp());
            OpenMesh::VertexHandle initialVertex = newUp.to();
            waveCollapse(waveFront);
            if (!flood(waveFront, submergedFaces, initialVertex)){
                return Tangle::errorTangle();
            }
            double area = submergedFaces.size();
            double bound = ceil( area/l + (l+1)/2. );
            minIntersect = std::max( minIntersect, static_cast<unsigned long>(bound) );
            if (minIntersect > maxIntersect) {
                return Tangle::errorTangle();
            }

            up.push_back(newUp);
            down.push_back(newDown);
        }
    }
    // No tangle could be constructed
    return Tangle::errorTangle();
}

bool QuadModifier::flood(std::vector<OpenMesh::HalfedgeHandle> &waveFront, std::vector<OpenMesh::FaceHandle> &submergedFaces, OpenMesh::VertexHandle initialVertex)
{
    std::vector< OpenMesh::HalfedgeHandle > oldWave( waveFront );
    waveFront.clear();
    // Extend the old wave front along all even indexed vertices
    for( unsigned long i = 1; i < oldWave.size(); i=i+2) {
        auto start = OpenMesh::make_smart(oldWave[i-1], mesh_);
        auto finish = OpenMesh::make_smart(oldWave[i], mesh_);
        while (start.opp() != finish) {
            waveFront.push_back(start.prev().opp());
            waveFront.push_back(start.prev().prev().opp());
            submergedFaces.push_back(start.face());
            start = start.next().opp();
        }
    }
    waveCollapse(waveFront);

    // At this point, every vertex should appear at most once
    // Otherwise, the wave front does not sweep out a disc
    std::set< OpenMesh::VertexHandle > vertices;
    vertices.insert( initialVertex );
    for (auto heh : waveFront) {
        OpenMesh::VertexHandle testVertex = mesh_.to_vertex_handle(heh);
        if (!vertices.insert(testVertex).second) {
            // The element already existed
            return false;
        }
    }
    return true;
}

void QuadModifier::waveCollapse(std::vector<OpenMesh::HalfedgeHandle> &waveFront)
{
    bool change = true;
    while( change ){
        change = false;
        for (unsigned long i = 1; i < waveFront.size(); i++){
            if( mesh_.opposite_halfedge_handle(waveFront[i-1]) == waveFront[i]){
                change = true;
                waveFront.erase( waveFront.begin()+i-1, waveFront.begin()+i+1);
                break;
            }
        }
    }
}

void QuadModifier::computeCycles()
{
    // find all singular vertices with 2 or 3 incident faces,
    // compute their QuadCycles and store them
    for (auto vh : mesh_.vertices())
    {
        unsigned int length = vh.valence();
        if (length < 4){
            QuadCycle small(mesh_, vh);
            int result = small.extendCycle();
            if (result >= 0){
                // Depending on the length the cycle is stored in different vectors
                if (small.halfedgeBoundary().size()==2) {
                    mesh_.property(cycle2_).push_back(small);
                } else if (small.halfedgeBoundary().size()==4) {
                    mesh_.property(cycle4_).push_back(small);
                } else if (small.halfedgeBoundary().size() == 6) {
                    // Check whether this is a proper 6-cycle
                    if (small.innerFaces().size() > 3) {
                        mesh_.property(cycle6_).push_back(small);
                    }
                } else {
                    // Should not happen
                }
            }
        }
    }
}

void QuadModifier::reduceCycle(QuadCycle cycle)
{
    if( cycle.halfedgeBoundary().size() == 2){
        reduce2Cycle(cycle);
    }
    if( cycle.halfedgeBoundary().size() == 4){
        reduce4Cycle(cycle);
    }
    if( cycle.halfedgeBoundary().size() == 6){
        reduce6Cycle(cycle);
    }
}



void QuadModifier::deleteCycleInterior(QuadCycle cycle)
{
    mesh_.request_face_status(); // Necessary to perform deletions
    mesh_.request_edge_status();
    mesh_.request_vertex_status();
    for( OpenMesh::FaceHandle face : cycle.innerFaces()){
        mesh_.delete_face(face, true);
    }
}

void QuadModifier::reduce4Cycle(QuadCycle cycle)
{
    // We need to figure out which type of replacement we have to do
    cycle.canonise();
    std::vector< OpenMesh::HalfedgeHandle > boundary = cycle.halfedgeBoundary();

    // Delete the inner part of the cycle
    deleteCycleInterior(cycle);

    // Replace it by something better
    if (cycle.getType() == QuadCycle::FOUR_ONE) {
        // Insert a single quad
        std::vector< OpenMesh::VertexHandle > newFace;
        for ( OpenMesh::HalfedgeHandle heh : boundary ) {
            newFace.push_back( mesh_.to_vertex_handle(heh));
        }
        mesh_.add_face(newFace);
    } else {
        // Collapse the 4-gon into 2 edges
        OpenMesh::VertexHandle id1, id2, ext1, ext2;
        id1 = mesh_.from_vertex_handle( boundary[0] );
        id2 = mesh_.from_vertex_handle( boundary[2] );
        ext1 = mesh_.from_vertex_handle( boundary[1] );
        ext2 = mesh_.from_vertex_handle( boundary[3] );

        // Add two faces, then delete the edge between them
        std::vector< OpenMesh::VertexHandle > t1, t2;
        t1.push_back(id1);
        t1.push_back(ext1);
        t1.push_back(id2);
        t2.push_back(id1);
        t2.push_back(id2);
        t2.push_back(ext2);
        mesh_.add_face(t1);
        mesh_.add_face(t2);
        OpenMesh::HalfedgeHandle newHalfedge = mesh_.find_halfedge(id1, id2);

        pointAverage( {id1,id2}, true );
        mesh_.collapse(newHalfedge);
    }
}

void QuadModifier::reduce6Cycle(QuadCycle cycle)
{
    QuadCycle::CYCLE_TYPE type = cycle.canonise();
    std::vector<OpenMesh::HalfedgeHandle> boundary = cycle.halfedgeBoundary();

    deleteCycleInterior(cycle);
    std::vector<OpenMesh::VertexHandle> vertices;
    for (unsigned int i = 0; i < boundary.size(); i++) {
        vertices.push_back( mesh_.from_vertex_handle(boundary[i]));
    }
    std::vector< OpenMesh::VertexHandle > face;
    OpenMesh::HalfedgeHandle heh1, heh2;

    // Distinguish the 5 types
    switch(type)
    {
        case QuadCycle::SIX_PINCH:
            face.clear();
            face.push_back( vertices[0] );
            face.push_back( vertices[1] );
            face.push_back( vertices[2] );
            mesh_.add_face(face);
            face.clear();
            face.push_back( vertices[2] );
            face.push_back( vertices[3] );
            face.push_back( vertices[4] );
            face.push_back( vertices[0] );
            mesh_.add_face(face);
            face.clear();
            face.push_back( vertices[4] );
            face.push_back( vertices[5] );
            face.push_back( vertices[0] );
            mesh_.add_face(face);

            heh1 = mesh_.find_halfedge(vertices[0],vertices[2]);
            heh2 = mesh_.find_halfedge(vertices[0],vertices[4]);
            pointAverage( {vertices[0], vertices[2], vertices[4] }, true );

            mesh_.collapse(heh1);
            mesh_.collapse(heh2);
            break;
        case QuadCycle::SIX_FLAT:
            face.clear();
            face.push_back( vertices[0] );
            face.push_back( vertices[1] );
            face.push_back( vertices[2] );
            mesh_.add_face(face);
            face.clear();
            face.push_back( vertices[0] );
            face.push_back( vertices[2] );
            face.push_back( vertices[3] );
            face.push_back( vertices[5] );
            mesh_.add_face(face);
            face.clear();
            face.push_back( vertices[3] );
            face.push_back( vertices[4] );
            face.push_back( vertices[5] );
            mesh_.add_face(face);

            heh1 = mesh_.find_halfedge(vertices[0],vertices[2]);
            pointAverage( {vertices[0], vertices[2]}, true );
            heh2 = mesh_.find_halfedge(vertices[3],vertices[5]);
            pointAverage( {vertices[3], vertices[5]}, true );

            mesh_.collapse(heh1);
            mesh_.collapse(heh2);
            break;
        case QuadCycle::SIX_ONE:
            face.clear();
            face.push_back( vertices[0] );
            face.push_back( vertices[1] );
            face.push_back( vertices[2] );
            mesh_.add_face(face);
            face.clear();
            face.push_back( vertices[2] );
            face.push_back( vertices[3] );
            face.push_back( vertices[4] );
            face.push_back( vertices[5] );
            face.push_back( vertices[0] );
            mesh_.add_face(face);

            heh1 = mesh_.find_halfedge(vertices[0],vertices[2]);
            pointAverage( { vertices[0], vertices[2] }, true );
            mesh_.collapse(heh1);
            break;
        case QuadCycle::SIX_TWO:
            face.clear();
            face.push_back( vertices[0] );
            face.push_back( vertices[1] );
            face.push_back( vertices[2] );
            face.push_back( vertices[5] );
            mesh_.add_face(face);

            face.clear();
            face.push_back( vertices[2] );
            face.push_back( vertices[3] );
            face.push_back( vertices[4] );
            face.push_back( vertices[5] );
            mesh_.add_face(face);
            break;
        case QuadCycle::SIX_THREE: // We choose one of the two options
            auto point = pointAverage( {vertices[0], vertices[2], vertices[4]}, false);
            OpenMesh::VertexHandle newVertex = mesh_.add_vertex(point);

            face.clear();
            face.push_back( vertices[0] );
            face.push_back( vertices[1] );
            face.push_back( vertices[2] );
            face.push_back( newVertex );
            mesh_.add_face(face);

            face.clear();
            face.push_back( vertices[2] );
            face.push_back( vertices[3] );
            face.push_back( vertices[4] );
            face.push_back( newVertex );
            mesh_.add_face(face);

            face.clear();
            face.push_back( vertices[4] );
            face.push_back( vertices[5] );
            face.push_back( vertices[0] );
            face.push_back( newVertex );
            mesh_.add_face(face);
            break;
    }
}

OpenMesh::VectorT<double,3> QuadModifier::pointAverage(std::vector<OpenMesh::VertexHandle> vertices, bool reset)
{
    if (vertices.size() == 0) {
        throw std::runtime_error("Given list of vertices is empty.");
    }
    OpenMesh::VectorT<double,3> point = {0.,0.,0.};
    for (unsigned int i = 0; i < vertices.size(); i++) {
        point = point + mesh_.point(vertices[i]);
    }
    point = point / vertices.size();

    if (reset) {
        for (OpenMesh::VertexHandle vh : vertices) {
            mesh_.set_point(vh, point);
        }
    }
    return point;
}

void QuadModifier::threeTwist(OpenMesh::VertexHandle vh)
{
    if( mesh_.valence(vh) != 3) {
        return;
    }

    QuadCycle cyc(mesh_, vh);
    std::vector< OpenMesh::VertexHandle > vertices;
    for( OpenMesh::HalfedgeHandle heh : cyc.halfedgeBoundary()) {
        vertices.push_back(mesh_.to_vertex_handle(heh));
    }

    auto newPoint = pointAverage( {vh}, false);
    deleteCycleInterior(cyc);

    OpenMesh::VertexHandle newVertex = mesh_.add_vertex(newPoint);
    std::vector< OpenMesh::VertexHandle > face;

    face.clear();
    face.push_back(vertices[0]);
    face.push_back(vertices[1]);
    face.push_back(vertices[2]);
    face.push_back(newVertex);
    mesh_.add_face(face);

    face.clear();
    face.push_back(vertices[2]);
    face.push_back(vertices[3]);
    face.push_back(vertices[4]);
    face.push_back(newVertex);
    mesh_.add_face(face);

    face.clear();
    face.push_back(vertices[4]);
    face.push_back(vertices[5]);
    face.push_back(vertices[0]);
    face.push_back(newVertex);
    mesh_.add_face(face);
}

bool QuadModifier::untangle()
{
    // 4-cycles have to be reduced with priority
    std::vector< QuadCycle > fourCycles = mesh_.property(cycle4_);
    if( fourCycles.size() > 0 ){
        reduce4Cycle(fourCycles[0]);
        return true;
    }

    // secondary priority are the 6-cycles with multiple inner vertices
    std::vector< QuadCycle > sixCycles = mesh_.property(cycle6_);
    for( QuadCycle cyc : sixCycles ) {
        if ( cyc.innerFaces().size() > 3 ) {
            reduce6Cycle(cyc);
            return true;
        }
    }

    // if no cycle can be reduced, we check the smallest tangle
    std::set< Tangle >::iterator it = mesh_.property(tangles_).begin();
    if( it == mesh_.property(tangles_).end() ) {
        return false;
    }
    Tangle t = *it;
    for (OpenMesh::HalfedgeHandle heh : t.boundary_) {
        // Search for vertices with valence 3 and twist them
        OpenMesh::VertexHandle vh = mesh_.to_vertex_handle(heh);
        if( mesh_.valence(vh) == 3 ) {
            threeTwist(vh);
            return true;
        }
    } // This search has to terminate
    throw std::runtime_error("Found a tangle without singularity on boundary.");
}

bool QuadModifier::untangleBigon(Tangle tangle)
{
    if (tangle.boundary_.size() == 2) {
        // The tangle is a 6-cycle
        QuadCycle cyc(mesh_, mesh_.from_vertex_handle(tangle.boundary_[0]));
        cyc.extendCycle();
        reduce6Cycle(cyc);
        return true;
    }

    if( tangle.area_ == 0) {
        return untangleBigonAreaZero(tangle);
    } else {
        return untangleBigonGeneric(tangle);
    }

}

bool QuadModifier::untangleBigonGeneric(Tangle tangle)
{
    // We need to store the information of this bigon to find
    // it again after the reduction
    auto bound0 = OpenMesh::make_smart(tangle.boundary_[0], mesh_);
    auto boundN = OpenMesh::make_smart(tangle.boundary_[tangle.boundary_.size()-1], mesh_);
    OpenMesh::HalfedgeHandle first = boundN.next().opp();
    OpenMesh::HalfedgeHandle second = bound0.prev().opp();

    // Reduce the bigon
    for (unsigned int i = 0; i < tangle.boundary_.size()-1; i++) {
        OpenMesh::HalfedgeHandle heh = tangle.boundary_[i];
        // Search for vertices with valence 3 and twist them
        OpenMesh::VertexHandle vh = mesh_.to_vertex_handle(heh);
        if( mesh_.valence(vh) == 3 ) {
            threeTwist(vh);
            // Find the bigon again
            computeDualLoops();
            Tangle newTangle = growTangle(first, second);
            if (newTangle.success_ == false) {
                throw std::runtime_error("Smaller bigon could not be constructed.");
            }
            return untangleBigon(newTangle);
        }
    }
    return false;
}

bool QuadModifier::untangleBigonAreaZero(Tangle tangle)
{
    OpenMesh::HalfedgeHandle first = tangle.boundary_[0];
    unsigned long len = tangle.boundary_.size()-1;
    OpenMesh::HalfedgeHandle last = mesh_.opposite_halfedge_handle( tangle.boundary_[len] );

    quadReduction3to2(first);
    quadReduction3to2(last);
    return true;
}

void QuadModifier::quadReduction3to2(OpenMesh::HalfedgeHandle heh)
{
    std::vector< OpenMesh::FaceHandle > oldFaces;
    std::vector< OpenMesh::VertexHandle > newFace1, newFace2;
    OpenMesh::HalfedgeHandle current = mesh_.next_halfedge_handle(heh);
    OpenMesh::HalfedgeHandle check = current;

    // Traverse first quad
    newFace1.push_back( mesh_.from_vertex_handle(current) );
    oldFaces.push_back( mesh_.face_handle(current) );
    current = mesh_.next_halfedge_handle(current);
    newFace1.push_back( mesh_.from_vertex_handle(current) );

    // Traverse second quad
    current = mesh_.next_halfedge_handle(current);
    newFace1.push_back( mesh_.from_vertex_handle(current) );
    oldFaces.push_back( mesh_.face_handle(current) );
    newFace1.push_back( mesh_.to_vertex_handle(current) );
    current = mesh_.next_halfedge_handle(current);
    newFace2.push_back( mesh_.from_vertex_handle(current) );

    // Traverse third quad
    current = mesh_.next_halfedge_handle(current);
    newFace2.push_back( mesh_.from_vertex_handle(current) );
    oldFaces.push_back( mesh_.face_handle(current) );
    current = mesh_.next_halfedge_handle(current);
    newFace2.push_back( mesh_.from_vertex_handle(current) );
    newFace2.push_back( mesh_.to_vertex_handle(current) );

    // Check
    if( check != mesh_.next_halfedge_handle(current) ) {
        std::runtime_error("quadReduction3to2 can only be called for valence-3-vertices");
    }

    // Remove old faces
    for( OpenMesh::FaceHandle fh : oldFaces ) {
        mesh_.delete_face(fh, true);
    }

    // Insert new faces
    mesh_.add_face(newFace1);
    mesh_.add_face(newFace2);
}

void QuadModifier::reduceSimpleDualLoops()
{
    bool reduced = true;
    while (reduced)
    {
        reduced = false;
        // Find a simple dual loop (no self-intersections, only regular vertices on one side)
        std::vector< OpenMesh::HalfedgeHandle > simpleLoop;
        for (unsigned int i = 1; i < mesh_.property(dualLoops_).size(); i++){
            auto loop = mesh_.property(dualLoops_)[i];

            bool intersect = false;
            bool to_regular = true;
            bool from_regular = true;
            for (unsigned long i=0; i < loop.size() && not intersect; i++){
                for (unsigned long j = i+1; j < loop.size() && not intersect; j ++){
                    if (mesh_.face_handle(loop[i]) == mesh_.face_handle(loop[j])) {
                        intersect = true;
                    }
                }
                if (mesh_.valence(mesh_.to_vertex_handle(loop[i])) != 4) {
                    to_regular = false;
                }
                if (mesh_.valence(mesh_.from_vertex_handle(loop[i])) != 4) {
                    from_regular = false;
                }
            }

            if (not intersect and (to_regular or from_regular)){
                simpleLoop = loop;
                reduced = true;
                break;
            }
        }

        if (reduced) {
            // We reduce the simple loop
            reduceDualLoop(simpleLoop);
            // re-analyse the mesh for the next iteration
            analyseMesh();
        }
    }
}

void QuadModifier::reduceDualLoop(std::vector<OpenMesh::HalfedgeHandle> loop)
{
    // We reduce the simple loop
    for (unsigned long i = 0; i < loop.size(); i++){
        mesh_.collapse( loop[i] );
    }
    analyseMesh();
}

std::vector< std::vector< OpenMesh::HalfedgeHandle > > QuadModifier::computeHomotopyBasis( OpenMesh::VertexHandle vertex )
{
    // Pseudo code from https://www.cs.cmu.edu/~kmcrane/Projects/LoopsOnSurfaces/

    // Compute a spanning tree of the edges
    OpenMesh::EPropHandleT< bool > edgeInTree;
    mesh_.add_property( edgeInTree );
    OpenMesh::VPropHandleT< bool > vertexInTree;
    mesh_.add_property( vertexInTree );
    OpenMesh::VPropHandleT< OpenMesh::VertexHandle > rootDirection;
    mesh_.add_property( rootDirection );
    for (OpenMesh::VertexHandle vh : mesh_.vertices()) {
        mesh_.property(vertexInTree, vh) = false;
    }
    for (OpenMesh::EdgeHandle eh : mesh_.edges()) {
        mesh_.property(edgeInTree, eh) = false;
    }

    std::deque< OpenMesh::VertexHandle > todo;
    todo.push_back(vertex);
    mesh_.property(vertexInTree, vertex) = true;
    mesh_.property(rootDirection, vertex) = vertex;
    while (!todo.empty()) {
        OpenMesh::VertexHandle next = todo.front();
        todo.pop_front();
        // check all incident edges
        for (auto hehIter = mesh_.voh_begin(next); hehIter != mesh_.voh_end(next); hehIter++) {
            OpenMesh::EdgeHandle edge = mesh_.edge_handle(*hehIter);
            if( !mesh_.property(edgeInTree, edge)) {
                // check adjacent vertex
                OpenMesh::VertexHandle partner = mesh_.to_vertex_handle(*hehIter);
                if (!mesh_.property(vertexInTree, partner)) {
                    // Add the vertex to the tree
                    todo.push_back(partner);
                    mesh_.property( vertexInTree, partner) = true;
                    mesh_.property( edgeInTree, edge) = true;
                    mesh_.property( rootDirection, partner) = next;
                }
            }
        }
    }

    // Compute the dual tree
    OpenMesh::EPropHandleT< bool > edgeInDualTree;
    mesh_.add_property(edgeInDualTree);
    OpenMesh::FPropHandleT< bool > faceInDualTree;
    mesh_.add_property(faceInDualTree);
    for (OpenMesh::EdgeHandle eh : mesh_.edges()) {
        mesh_.property( edgeInDualTree, eh) = false;
    }
    for (OpenMesh::FaceHandle fh : mesh_.faces()) {
        mesh_.property(faceInDualTree, fh) = false;
    }
    std::deque< OpenMesh::FaceHandle > nextFaces;
    nextFaces.push_back( *mesh_.faces_begin() );
    mesh_.property( faceInDualTree, nextFaces.back() ) = true;
    while (!nextFaces.empty()) {
        OpenMesh::FaceHandle next = nextFaces.front();
        nextFaces.pop_front();
        for (auto hehIter = mesh_.fh_begin(next); hehIter != mesh_.fh_end(next); hehIter++ ) {
            OpenMesh::EdgeHandle edge = mesh_.edge_handle(*hehIter);
            if (!mesh_.property(edgeInDualTree, edge) && !mesh_.property(edgeInTree, edge)) {
                OpenMesh::FaceHandle partner = mesh_.face_handle( mesh_.opposite_halfedge_handle(*hehIter));
                if (!mesh_.property(faceInDualTree, partner)) {
                    nextFaces.push_back(partner);
                    mesh_.property(faceInDualTree, partner) = true;
                    mesh_.property(edgeInDualTree, edge) = true;
                }
            }
        }
    }

    // Find all edges that lie in neither of the two trees
    std::vector< std::vector< OpenMesh::HalfedgeHandle > > loops;
    for (OpenMesh::EdgeHandle edge : mesh_.edges()) {
        if (!mesh_.property(edgeInTree, edge) && !mesh_.property(edgeInDualTree, edge)) {
            // extend this edge to a closed loop
            std::vector< OpenMesh::HalfedgeHandle > pathFrom, pathTo;
            OpenMesh::HalfedgeHandle heh = mesh_.halfedge_handle(edge,0);
            OpenMesh::VertexHandle from = mesh_.from_vertex_handle(heh);
            OpenMesh::VertexHandle to = mesh_.to_vertex_handle(heh);
            pathTo.push_back( heh );
            while (mesh_.property(rootDirection, from) != from) {
                OpenMesh::VertexHandle next = mesh_.property(rootDirection, from);
                OpenMesh::HalfedgeHandle between = mesh_.find_halfedge(next, from);
                pathFrom.push_back( between );
                from = next;
            }
            while (mesh_.property(rootDirection, to) != to) {
                OpenMesh::VertexHandle next = mesh_.property(rootDirection, to);
                OpenMesh::HalfedgeHandle between = mesh_.find_halfedge(to, next);
                pathTo.push_back( between );
                to = next;
            }
            std::reverse(pathFrom.begin(), pathFrom.end());
            pathFrom.insert( pathFrom.end(), pathTo.begin(), pathTo.end() );
            loops.push_back(pathFrom);
        }
    }

    // Remove all temporary properties
    mesh_.remove_property( edgeInTree );
    mesh_.remove_property( vertexInTree );
    mesh_.remove_property( rootDirection );
    mesh_.remove_property( edgeInDualTree );
    mesh_.remove_property( faceInDualTree );

    return loops;
}

void QuadModifier::computeHomologyBasis()
{
    // Pseudo code from https://www.cs.cmu.edu/~kmcrane/Projects/LoopsOnSurfaces/
    long euler = std::distance( mesh_.vertices_begin(), mesh_.vertices_end())
            - std::distance( mesh_.edges_begin(), mesh_.edges_end())
            + std::distance( mesh_.faces_begin(), mesh_.faces_end());
    long genus = 1 - euler/2;
    if (genus == 0) {
        std::vector< std::vector< OpenMesh::HalfedgeHandle > > homology;
        mesh_.property(homologyBasis_) = homology;
        return;
    }

    // Compute homotopy bases for all vertices
    std::vector< std::vector< OpenMesh::HalfedgeHandle > > loops;
    for( OpenMesh::VertexHandle vh : mesh_.vertices()) {
        std::vector< std::vector< OpenMesh::HalfedgeHandle > > homotopy = computeHomotopyBasis(vh);
        loops.insert(loops.end(), homotopy.begin(), homotopy.end());
    }
    // Sort them by length
    struct smallerLength
    {
        inline bool operator() (const std::vector< OpenMesh::HalfedgeHandle > first, const std::vector< OpenMesh::HalfedgeHandle > second)
        {
            return first.size() < second.size();
        }
    };
    std::sort( loops.begin(), loops.end(), smallerLength() );


    std::vector< std::vector< OpenMesh::HalfedgeHandle > > homology;
    homology.push_back( *loops.begin() );
    unsigned long currentIndex = 0;
    std::set< OpenMesh::EdgeHandle > forbiddenEdges;

    OpenMesh::FPropHandleT< bool > facesIn;
    mesh_.add_property(facesIn);
    for( long index = 0; index < genus; index++) {
        for( OpenMesh::HalfedgeHandle heh : homology[homology.size()-1] ) {
            forbiddenEdges.insert( mesh_.edge_handle(heh));
        }

        // Add the loop of smallest length that does not cut the surface into disjoint components
        for( unsigned long j = 1; currentIndex + j < loops.size(); j++) {
            std::vector< OpenMesh::HalfedgeHandle > currentLoop = loops[currentIndex+j];

            // Set all faces to "not visited"
            for( OpenMesh::FaceHandle fh : mesh_.faces() ){
                mesh_.property(facesIn, fh) = false;
            }
            std::deque< OpenMesh::FaceHandle > nextFaces;
            nextFaces.push_back( *mesh_.faces_sbegin() );

            // Perform a breadth-first search
            while( !nextFaces.empty() ) {
                OpenMesh::FaceHandle nowFace = nextFaces.front();
                nextFaces.pop_front();
                for (auto hehIter = mesh_.fh_begin(nowFace); hehIter != mesh_.fh_end(nowFace); hehIter++ ) {
                    OpenMesh::EdgeHandle connectingEdge = mesh_.edge_handle(*hehIter);
                    if( forbiddenEdges.find(connectingEdge) != forbiddenEdges.end() ) {
                        continue;
                    }
                    bool foundInNewLoop = false;
                    for( OpenMesh::HalfedgeHandle h : currentLoop ) {
                        if( mesh_.edge_handle(h) == connectingEdge ) {
                            foundInNewLoop = true;
                        }
                    }
                    if( foundInNewLoop ) {
                        continue;
                    }

                    // Compute the neighbouring face
                    OpenMesh::FaceHandle neighbour = mesh_.face_handle( mesh_.opposite_halfedge_handle( *hehIter ) );
                    if( !mesh_.property(facesIn, neighbour)) {
                        mesh_.property(facesIn, neighbour) = true;
                        nextFaces.push_back(neighbour);
                    }
                }
            }

            // Check whether the remaining surface is connected
            bool isConnected = true;
            for( OpenMesh::FaceHandle fh : mesh_.faces() ) {
                if (!mesh_.property(facesIn, fh)) {
                    isConnected = false;
                    break;
                }
            }
            if( isConnected ) {
                homology.push_back( currentLoop );
                currentIndex += j;
            }
        }
    }
    mesh_.remove_property(facesIn);

    mesh_.property(homologyBasis_) = homology;
}

void QuadModifier::computeSpirals()
{
    std::vector< std::vector< std::vector< std::vector< unsigned long > > > > spirals;
    std::vector< std::vector< std::vector< unsigned long > > > emptyFirst;
    spirals.push_back(emptyFirst);
    spirals.reserve( mesh_.property(dualLoops_).size() );

    for( unsigned long loopIndex = 1; loopIndex < mesh_.property(dualLoops_).size(); loopIndex++ ) {
        std::vector< OpenMesh::HalfedgeHandle > dualLoop = mesh_.property(dualLoops_)[loopIndex];
        std::vector< std::vector< std::vector< unsigned long > > > loopSpirals;
        loopSpirals.reserve( mesh_.property(homologyBasis_).size() );

        for( unsigned long homIndex = 0; homIndex < mesh_.property(homologyBasis_).size(); homIndex++ ) {
            std::vector< OpenMesh::HalfedgeHandle > generator = mesh_.property(homologyBasis_)[homIndex];

            // Find all directed intersections between dualLoop and generator
            std::vector< unsigned long > positions;
            std::vector< int > directions;
            for( unsigned long pos = 0; pos < generator.size(); pos++ ) {
                bool stillSearching = true;
                for( unsigned long dualPos = 0; dualPos < dualLoop.size() && stillSearching; dualPos++) {
                    OpenMesh::HalfedgeHandle check = dualLoop[dualPos];
                    if( generator[pos] == check ) {
                        positions.push_back(pos);
                        directions.push_back(+1);
                        stillSearching = false;
                    } else if (generator[pos] == mesh_.opposite_halfedge_handle(check) ) {
                        positions.push_back(pos);
                        directions.push_back(-1);
                        stillSearching = false;
                    }
                }
            }

            // Compute the spirals
            unsigned long length = positions.size();
            std::vector< std::vector< unsigned long > > loopGenSpirals;
            positions.insert( positions.end(), positions.begin(), positions.end() );
            directions.insert( directions.end(), directions.begin(), directions.end() );
            for( unsigned long i = 0; i < length; i++) {
                unsigned long j = 0;
                int sum = directions[i];
                bool success = false;
                // We only compute minimal intervals. Thus, a sign-flip cannot happen.
                if (sum == +1) {
                    while( j < length-1 && sum < 2 && sum >= 0 ) {
                        j++;
                        sum += directions[i+j];
                    }
                    if( sum == 2 ) {
                        success = true;
                    }
                } else {
                    while( j < length-1 && sum > -2 && sum <= 0 ) {
                        j++;
                        sum += directions[i+j];
                    }
                    if( sum == -2 ) {
                        success = true;
                    }
                }

                if( success ) {
                    std::vector< unsigned long > spiralPositions;
                    spiralPositions.push_back(positions[i]);
                    spiralPositions.push_back(positions[i+j]);
                    loopGenSpirals.push_back( spiralPositions );
                }
            }

            loopSpirals.push_back( loopGenSpirals );
        }
        spirals.push_back( loopSpirals );
    }

    mesh_.property( spirals_ ) = spirals;
}

void QuadModifier::computeSpiralPaths()
{
    auto spirals = mesh_.property(spirals_);
    std::vector< std::vector< OpenMesh::HalfedgeHandle > > spiralPaths;

    for( unsigned long dualIndex=1; dualIndex < mesh_.property(dualLoops_).size(); dualIndex++) {
        auto dualSpirals = spirals[dualIndex];

        for( unsigned long homIndex = 0; homIndex < mesh_.property(homologyBasis_).size(); homIndex++ ) {
            auto homGenerator = mesh_.property(homologyBasis_)[homIndex];
            auto dualHomSpirals = dualSpirals[homIndex];

            for( std::vector<unsigned long> concreteSpiral : dualHomSpirals ) {
                unsigned long first = concreteSpiral[0];
                unsigned long second = concreteSpiral[1];

                // Since the positions are already minimal, we compute the path from the first to the second entry
                std::vector< OpenMesh::HalfedgeHandle > path;
                if( first < second ) {
                    for( unsigned long step = first; step <= second; step++ ) {
                        path.push_back( homGenerator[step] );
                    }
                }
                else {
                    for( unsigned long step = first; step < homGenerator.size(); step++ ) {
                        path.push_back( homGenerator[step]);
                    }
                    for( unsigned long step = 0; step <= second; step++ ) {
                        path.push_back( homGenerator[step] );
                    }
                }
                spiralPaths.push_back(path);
            }
        }
    }
    mesh_.property( spiralPaths_ ) = spiralPaths;
}

void QuadModifier::unspool(std::vector<OpenMesh::HalfedgeHandle> path)
{
    std::vector<OpenMesh::HalfedgeHandle> oppPath;
    for( OpenMesh::HalfedgeHandle heh : path ){
        oppPath.push_back( mesh_.opposite_halfedge_handle(heh) );
    }

    std::vector< OpenMesh::VectorT<double,3> > newTop, newBottom;
    for( unsigned long index = 0; index < path.size()-1; index++ ) {
        // We determine for each vertex on the path, which halfedges enter from
        // the top and which from the bottom
        std::vector< OpenMesh::HalfedgeHandle > top, bottom;
        top.push_back( path[index] );
        while( mesh_.next_halfedge_handle(top[top.size()-1]) != path[index+1]) {
            OpenMesh::HalfedgeHandle next = mesh_.opposite_halfedge_handle( mesh_.next_halfedge_handle( top[top.size()-1]));
            top.push_back(next);
        }

        bottom.push_back( oppPath[index+1]);
        while( mesh_.next_halfedge_handle(bottom[bottom.size()-1]) != oppPath[index]) {
            OpenMesh::HalfedgeHandle next = mesh_.opposite_halfedge_handle( mesh_.next_halfedge_handle( bottom[bottom.size()-1]));
            bottom.push_back(next);
        }

        // We compute the new top and bottom vertices
        std::vector< OpenMesh::VertexHandle > topVertices, bottomVertices;
        for (OpenMesh::HalfedgeHandle heh : top) {
            topVertices.push_back( mesh_.from_vertex_handle(heh));
        }
        topVertices.push_back(mesh_.to_vertex_handle(path[index+1]));
        auto topPoint = pointAverage(topVertices, false);

        for (OpenMesh::HalfedgeHandle heh : bottom) {
            bottomVertices.push_back( mesh_.from_vertex_handle(heh));
        }
        bottomVertices.push_back(mesh_.to_vertex_handle(oppPath[index]));
        auto bottomPoint = pointAverage(bottomVertices, false);

        newTop.push_back(topPoint);
        newBottom.push_back(bottomPoint);
    }

    int val = mesh_.valence( mesh_.from_vertex_handle(path[0]));

    // Extend the first quad (the only asymmetric one)
    extendQuad(path[0],path[1],newTop[0],newBottom[0]);
    path[1] = mesh_.opposite_halfedge_handle( mesh_.next_halfedge_handle(path[1]));
    for( unsigned long index = 1; index < path.size()-1; index++) {
        auto point = mesh_.point( mesh_.to_vertex_handle(path[index]));

        extendQuad(path[index],path[index+1],newTop[index], point);
        path[index+1] =  mesh_.opposite_halfedge_handle( mesh_.next_halfedge_handle(path[index+1]));

        extendQuad(oppPath[index+1],oppPath[index],newBottom[index],point);
        oppPath[index+1] = mesh_.opposite_halfedge_handle(mesh_.prev_halfedge_handle(oppPath[index+1]));
    }
}

// Copied from TriConnectivity.cc
OpenMesh::HalfedgeHandle QuadModifier::insert_loop(OpenMesh::HalfedgeHandle _hh)
{
    OpenMesh::HalfedgeHandle  h0(_hh);
    OpenMesh::HalfedgeHandle  o0(mesh_.opposite_halfedge_handle(h0));

    OpenMesh::VertexHandle    v0(mesh_.to_vertex_handle(o0));
    OpenMesh::VertexHandle    v1(mesh_.to_vertex_handle(h0));

    OpenMesh::HalfedgeHandle  h1 = mesh_.new_edge(v1, v0);
    OpenMesh::HalfedgeHandle  o1 = mesh_.opposite_halfedge_handle(h1);

    OpenMesh::FaceHandle      f0 = mesh_.face_handle(h0);
    OpenMesh::FaceHandle      f1 = mesh_.new_face();

    // halfedge -> halfedge
    mesh_.set_next_halfedge_handle(mesh_.prev_halfedge_handle(h0), o1);
    mesh_.set_next_halfedge_handle(o1, mesh_.next_halfedge_handle(h0));
    mesh_.set_next_halfedge_handle(h1, h0);
    mesh_.set_next_halfedge_handle(h0, h1);

    // halfedge -> face
    mesh_.set_face_handle(o1, f0);
    mesh_.set_face_handle(h0, f1);
    mesh_.set_face_handle(h1, f1);

    // face -> halfedge
    mesh_.set_halfedge_handle(f1, h0);
    if (f0.is_valid())
      mesh_.set_halfedge_handle(f0, o1);


    // vertex -> halfedge
    mesh_.adjust_outgoing_halfedge(v0);
    mesh_.adjust_outgoing_halfedge(v1);

    return h1;
}

// Copied from TriConnectivity.cc
OpenMesh::HalfedgeHandle QuadModifier::insert_edge(OpenMesh::VertexHandle _vh, OpenMesh::HalfedgeHandle _h0, OpenMesh::HalfedgeHandle _h1)
{
    assert(_h0.is_valid() && _h1.is_valid());

    OpenMesh::VertexHandle  v0 = _vh;
    OpenMesh::VertexHandle  v1 = mesh_.to_vertex_handle(_h0);

    assert( v1 == mesh_.to_vertex_handle(_h1));

    OpenMesh::HalfedgeHandle v0v1 = mesh_.new_edge(v0, v1);
    OpenMesh::HalfedgeHandle v1v0 = mesh_.opposite_halfedge_handle(v0v1);



    // vertex -> halfedge
    mesh_.set_halfedge_handle(v0, v0v1);
    mesh_.set_halfedge_handle(v1, v1v0);


    // halfedge -> halfedge
    mesh_.set_next_halfedge_handle(v0v1, mesh_.next_halfedge_handle(_h0));
    mesh_.set_next_halfedge_handle(_h0, v0v1);
    mesh_.set_next_halfedge_handle(v1v0, mesh_.next_halfedge_handle(_h1));
    mesh_.set_next_halfedge_handle(_h1, v1v0);


    // halfedge -> vertex
    for (auto vih_it(mesh_.vih_iter(v0)); vih_it.is_valid(); ++vih_it)
      mesh_.set_vertex_handle(*vih_it, v0);


    // halfedge -> face
    mesh_.set_face_handle(v0v1, mesh_.face_handle(_h0));
    mesh_.set_face_handle(v1v0, mesh_.face_handle(_h1));


    // face -> halfedge
    if (mesh_.face_handle(v0v1).is_valid())
      mesh_.set_halfedge_handle(mesh_.face_handle(v0v1), v0v1);
    if (mesh_.face_handle(v1v0).is_valid())
      mesh_.set_halfedge_handle(mesh_.face_handle(v1v0), v1v0);


    // vertex -> halfedge
    mesh_.adjust_outgoing_halfedge(v0);
    mesh_.adjust_outgoing_halfedge(v1);


    return v0v1;
}

void QuadModifier::vanish_edge(OpenMesh::EdgeHandle _eh)
{
    OpenMesh::HalfedgeHandle h1 = mesh_.halfedge_handle(_eh, 0);
    OpenMesh::HalfedgeHandle h2 = mesh_.halfedge_handle(_eh, 1);

    std::vector< OpenMesh::VertexHandle > vertices;
    for( OpenMesh::HalfedgeHandle heh = mesh_.next_halfedge_handle(h1); h1 != heh; heh = mesh_.next_halfedge_handle(heh)) {
        vertices.push_back( mesh_.to_vertex_handle(heh) );
    }
    for( OpenMesh::HalfedgeHandle heh = mesh_.next_halfedge_handle(h2); h2 != heh; heh = mesh_.next_halfedge_handle(heh)) {
        vertices.push_back( mesh_.to_vertex_handle(heh) );
    }

    OpenMesh::FaceHandle f1 = mesh_.face_handle(h1);
    OpenMesh::FaceHandle f2 = mesh_.face_handle(h2);
    mesh_.delete_face(f1, true);
    mesh_.delete_face(f2, true);

    mesh_.add_face(vertices);
}

void QuadModifier::extendQuad(OpenMesh::HalfedgeHandle _h0, OpenMesh::HalfedgeHandle _h1, OpenMesh::VectorT<double, 3> _pUp, OpenMesh::VectorT<double, 3> _pDown)
{
    insert_loop(_h0);
    OpenMesh::HalfedgeHandle h2 = insert_loop(_h1);
    OpenMesh::VertexHandle vh = mesh_.add_vertex(_pDown);
    mesh_.set_point( mesh_.to_vertex_handle(_h0), _pUp);
    OpenMesh::HalfedgeHandle heh = insert_edge(vh, _h0, h2);
    OpenMesh::EdgeHandle eh = mesh_.edge_handle(heh);
    vanish_edge( eh );
}
