/*
 * Author: Patrick Schmidt
 */
#include "Draw.hh"

#include <ACG/Scenegraph/TransformNode.hh>
#include <ACG/Scenegraph/MaterialNode.hh>
#include <ACG/Scenegraph/PointNode.hh>
#include <ACG/Scenegraph/LineNode.hh>

Draw::Draw()
    : translation_(0.0),
      transform_node_(nullptr)
{

}

Draw::~Draw()
{
    clear();
}

void Draw::set_translation(ACG::Vec3d _t)
{
    translation_ = _t;
    apply_translation();
}

void Draw::reset_translation()
{
    translation_ = ACG::Vec3d(0.0);
    apply_translation();
}

void Draw::apply_translation()
{
    transform_node()->loadIdentity();
    transform_node()->translate(translation_);
}

void Draw::clear()
{
    point_nodes_.clear();
    line_nodes_.clear();

    if (transform_node_)
        transform_node_->delete_subtree();

    transform_node_ = nullptr;
}

void Draw::point(ACG::Vec2d _p, Color _color, float _width/* = 10.0f*/)
{
    point(ACG::Vec3d(_p[0], 0.0, -_p[1]), _color, _width);
}

void Draw::point(ACG::Vec3d _p, Color _color, float _width/* = 10.0f*/)
{
    PointNode* node = point_node(_width);

    node->add_color(_color);
    node->add_point(_p);
    node->show();
}

void Draw::line(ACG::Vec2d _from, ACG::Vec2d _to, Color _color, float _width/* = 4.0f*/)
{
    return line(ACG::Vec3d(_from[0], 0.0, -_from[1]), ACG::Vec3d(_to[0], 0.0, -_to[1]), _color, _width);
}

void Draw::line(ACG::Vec3d _from, ACG::Vec3d _to, Color _color, float _width/* = 4.0f*/)
{
    LineNode* node = line_node(_width);

    node->add_color(_color);
    node->add_line(_from, _to);
    node->show();
}



Draw::TransformNode* Draw::transform_node()
{
    if (!transform_node_)
    {
        transform_node_ = new TransformNode();
        PluginFunctions::addGlobalNode(transform_node_);
        apply_translation();
    }

    return transform_node_;
}

ACG::SceneGraph::PointNode* Draw::point_node(float _width)
{
    // Already there?
    for (auto& node : point_nodes_)
    {
        auto* material_node = dynamic_cast<ACG::SceneGraph::MaterialNode*>(node->parent());
        if (material_node->point_size() == _width)
            return node;
    }

    // Add new node
    auto* material_node = new ACG::SceneGraph::MaterialNode(transform_node());
    material_node->enable_blending();

    auto* point_node = new ACG::SceneGraph::PointNode(material_node);
    material_node->set_point_size(_width);
    point_node->enablePicking(false);
    point_node->drawMode(ACG::SceneGraph::DrawModes::POINTS_COLORED);
    point_nodes_.push_back(point_node);

    return point_node;
}

ACG::SceneGraph::LineNode* Draw::line_node(float _width)
{
    // Already there?
    for (auto& node : line_nodes_)
    {
        if (node->line_width() == _width)
            return node;
    }

    // Add new node
    LineNode* node = new ACG::SceneGraph::LineNode(ACG::SceneGraph::LineNode::LineSegmentsMode, transform_node());
    node->set_line_width(_width);
    node->enablePicking(false);
    line_nodes_.push_back(node);

    return node;
}


